package a.test.testtest;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.NoteBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.function.IntBinaryOperator;

/**
 * Created by sasamijp on 2017/02/27.
 */

public class PlayerBlockPlaceEvent implements Listener {

    @EventHandler
    public void onBlockPlacing(BlockPlaceEvent event){
        Block block = event.getBlockPlaced();
        if(block.getType().equals(Material.LEVER)){
            int x = block.getX();
            int y = block.getY();
            int z = block.getZ();
            int angle = getNoteVector(block.getWorld(), x, y, z);
            switch (angle){
                case 0:
                    Testtest.pitches = getPitches(block.getWorld(), x, y, z, 0, 1);
                    break;
                case 1:
                    Testtest.pitches = getPitches(block.getWorld(), x, y, z, 1, 0);
                    break;
                case 2:
                    Testtest.pitches = getPitches(block.getWorld(), x, y, z, 0, -1);
                    break;
                case 3:
                    Testtest.pitches = getPitches(block.getWorld(), x, y, z, -1, 0);
                    break;
                default:
                    Bukkit.broadcastMessage("4");
                    break;
            }
            //Bukkit.broadcastMessage(Testtest.pitches.toString());
        }
//        if(block.getType().equals(Material.NOTE_BLOCK)){
//            int x = block.getX();
//            int y = block.getY();
//            int z = block.getZ();
//            boolean s = ((NoteBlock)block.getState()).getNote().getTone().isSharped(
//                    ((NoteBlock)block.getState()).getNote().getId()
//            );
//            int note = ((NoteBlock)block.getState()).getNote().getTone().ordinal();
//
//            int octave = ((NoteBlock)block.getState()).getNote().getOctave();
//            //int id = note.getTone();
//            Bukkit.broadcastMessage("o"+String.valueOf(s)+","+String.valueOf(note) + "," + String.valueOf(octave));
//            Block nblock = new Location(block.getWorld(), x, y, z+1).getBlock();
//            int id = Byte.toUnsignedInt(((NoteBlock)block.getState()).getNote().getId());
//            Bukkit.broadcastMessage("o"+String.valueOf(id));
//            Testtest.pitches.add(getPitch(id));
//
//            if (nblock.getType().equals(Material.NOTE_BLOCK)){
//                s = ((NoteBlock)nblock.getState()).getNote().getTone().isSharped(
//                        ((NoteBlock)nblock.getState()).getNote().getId()
//                );
//                note = ((NoteBlock)nblock.getState()).getNote().getTone().ordinal();
//                octave = ((NoteBlock)nblock.getState()).getNote().getOctave();
//                //int id = note.getTone();
//
//                id = Byte.toUnsignedInt(((NoteBlock)nblock.getState()).getNote().getId());
//                Bukkit.broadcastMessage("n"+String.valueOf(id));
//
//                //Bukkit.broadcastMessage("o"+String.valueOf(s)+","+String.valueOf(note) + "," + String.valueOf(octave));
//            }
//            //    x, z+1 x, z-1 x+1, z x-1, z
//        }
    }

    private ArrayList<Float> getPitches(World world, int x, int y, int z, int x_v, int z_v){
        x+=x_v;
        z+=z_v;
        Block block;
        ArrayList<Float> pitches = new ArrayList<>();
        int count = 0;
        while(true) {
            block = new Location(world, x, y, z).getBlock();
            if (block.getType().equals(Material.NOTE_BLOCK) || count == 1) {
                if(count <= 1) {
                    pitches.add(0F);
                }else{
                    pitches.add(getPitchByBlock(block));
                }
                x+=x_v;
                z+=z_v;
                if(block.getType().equals(Material.NOTE_BLOCK)) count = 0;
            }else{
                if(count > 1) {
                    break;
                }
                count++;
            }
        }
        //Bukkit.broadcastMessage(pitches.toString());
        //for(float p: pitches){

        //    Bukkit.broadcastMessage(String.valueOf(p));

        //}
        return pitches;
    }

    private int getNoteVector(World world, int x, int y, int z){
        if (new Location(world, x, y, z+1).getBlock().getType().equals(Material.NOTE_BLOCK)){
            return 0;
        }

        if (new Location(world, x+1, y, z).getBlock().getType().equals(Material.NOTE_BLOCK)){
            return 1;
        }

        if (new Location(world, x, y, z-1).getBlock().getType().equals(Material.NOTE_BLOCK)){
            return 2;
        }

        if (new Location(world, x-1, y, z).getBlock().getType().equals(Material.NOTE_BLOCK)){
            return 3;
        }
        return 4;
    }

    private float getPitchByBlock(Block block){
        int id = Byte.toUnsignedInt(((NoteBlock)block.getState()).getNote().getId());
        return getPitch(id);
    }

    private static float getPitch(int id) {
        switch (id) {
            case 0: return 0.5F; //F#
            case 1: return 0.53F;//G
            case 2: return 0.56F;//G#
            case 3: return 0.6F;//A
            case 4: return 0.63F;//A#
            case 5: return 0.67F;//B
            case 6: return 0.7F;//C
            case 7: return 0.76F;//C#
            case 8: return 0.8F;//D
            case 9: return 0.84F;//D#
            case 10: return 0.9F;//E
            case 11: return 0.94F;//F
            case 12: return 1.0F;//F#
            case 13: return 1.06F;//G
            case 14: return 1.12F;//G#
            case 15: return 1.18F;//A
            case 16: return 1.26F;//A#
            case 17: return 1.34F;//B
            case 18: return 1.42F;//C
            case 19: return 1.5F;//C#
            case 20: return 1.6F;//D
            case 21: return 1.68F;//D#
            case 22: return 1.78F;//E
            case 23: return 1.88F;//F
            case 24: return 2.0F;//F#
            default: return 0.0F;
        }
    }

}
