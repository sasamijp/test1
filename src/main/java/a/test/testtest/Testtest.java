package a.test.testtest;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Testtest extends JavaPlugin {
    private double x = 0.7;
    public static ArrayList<Float> pitches;

    @Override
    public void onEnable() {
        // Plugin startup logic
        pitches = new ArrayList<>();
        getServer().getPluginManager().registerEvents(new PlayerBlockPlaceEvent(), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("basic")) {

            if(sender instanceof Player) {
                Player player = (Player) sender;
                int lim = 100;
                SoundTask[] array = new SoundTask[lim];

                //SoundTask s = new SoundTask();
                for (float i = 0; i < lim; i++) {
                    array[(int)i] = new SoundTask();
                    array[(int)i].setSound(player.getLocation(), (float) (0.5 + Math.sin(i / 20)));
                    // sender.sendMessage(String.valueOf((float) 0.5 + Math.sin(i / 20)));
                    array[(int)i].runTaskLater(this, (long) (i));
                }
            }

            // Bukkit.getWorld("world").playSound(player.getLocation(), Sound.BLOCK_NOTE_HARP, 100, );

            //for(float p: pitches){
            sender.sendMessage(pitches.toString());

            //}
            //sender.sendMessage("うおおおおおおおおおお！！！！！！！！！！！！！！！！！！");
            return true;
        }
        // If this has happened the function will return true.
        // If this hasn't happened the value of false will be returned.
        return false;
    }
}
