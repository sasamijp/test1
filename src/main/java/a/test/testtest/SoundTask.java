package a.test.testtest;

/**
 * Created by sasamijp on 2017/02/27.
 */
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class SoundTask extends BukkitRunnable {
    private Location location;
    private float pitch;

    public void setSound(Location _location, float _pitch) {
        this.location = _location;
        this.pitch = _pitch;
    }


    @Override
    public void run() {
        Bukkit.getWorld("world").playSound(
                location,
                Sound.BLOCK_NOTE_HARP,
                100,
                pitch
        );
    }

}